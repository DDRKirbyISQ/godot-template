# Global Init singleton, used for initializing anything
# that needs to be done at startup regardless of scene

extends Node


func _ready():
	randomize()
	DialogBox.LoadPresets({
		# "player": {
		# 	"color": Color(0.5, 1.0, 1.0),
		# 	"soundLoop": preload("res://sfx/dialog_main.ogg"),
		# }
	})