extends CanvasLayer

const _menuScene: PackedScene = preload("res://scenes/menu-scene-cocoa-moss.tscn")
const _copyProtectScene: PackedScene = preload("res://scenes/copy-protect-scene-cocoa-moss.tscn")


func _ready() -> void:
	if SiteLockUrls.HasViolation():
		get_tree().change_scene_to(_copyProtectScene)
		return

	ScreenTransitioner.InstantOut()
	yield(ScreenTransitioner.TransitionIn(1.0, ScreenTransitioner.FADE), "completed")
	yield(Yields.TimeOrInputPressed(2, "ui_accept"), "completed")
	yield(ScreenTransitioner.TransitionOut(1.0, ScreenTransitioner.FADE), "completed")
	get_tree().change_scene_to(_menuScene)
	yield(ScreenTransitioner.TransitionIn(1.0, ScreenTransitioner.FADE), "completed")
