class_name SiteLockUrls

const kAllowedReferrers: Array = [
	"^cocoamoss\\.com/.*$",
	"^ddrkirby\\.com/.*$",
	"^katmengjia\\.com/.*$",
	"^ddrkirbyisq\\.itch\\.io/.*$",
]

const kAllowedUrls: Array = [
	"^file:\\/\\/\\/.*$",
	"^localhost[:\\/].*$",
	"^itch-cave:\\/\\/.*$",
]

static func HasViolation():
	return SiteLock.HasViolation(kAllowedReferrers, kAllowedUrls)
