extends CanvasLayer

const _mainScene: PackedScene = preload("res://scenes/main-scene.tscn")


func _ready() -> void:
	pass


func _process(delta) -> void:
	if Input.is_action_just_pressed("ui_accept"):
		yield(ScreenTransitioner.TransitionOut(1.0, ScreenTransitioner.DIAMONDS), "completed")
		get_tree().change_scene_to(_mainScene)
		yield(ScreenTransitioner.TransitionIn(1.0, ScreenTransitioner.DIAMONDS), "completed")
